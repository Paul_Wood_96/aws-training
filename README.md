<div align="center"><h1>WildRydes Tutorial on AWS</h1></div> 

This is a work through project I did going through the _AWS WildRydes_ tutorial avaialable 
[here](https://aws.amazon.com/getting-started/hands-on/build-serverless-web-app-lambda-apigateway-s3-dynamodb-cognito/)

Commands used in the Amazon CLI that where needed or that I discovered while working through the tutorial: 

```aws s3 sync s3://wildrydes-us-east-1/WebApplication/1_StaticWebHosting/website s3://pauls-simple-aws-website --region eu-central-1```

The above command is used to sync your _[AWS S3 Bucket.](https://docs.aws.amazon.com/AmazonS3/latest/gsg/SigningUpforS3.html)_
Int the case of this example Buckets are used to store our static website, this current directory. 

```aws s3 cp s3://pauls-simple-aws-website . --recursive --region eu-central-```

If you want to copy the directory into a local folder use the above command. You will notice the sync 
command copied files current stored on an AWS server, so we never really had them. 
